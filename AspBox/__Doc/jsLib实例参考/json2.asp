<!--#include file="inc/AspBox/Cls_AB.asp" -->
<%
AB.Use "sc"
Dim sc : Set sc = ab.sc.new
sc.Lang = "js"
sc.Add "function foo(){ var person = {name: {a:""zhangsan""}, pass: ""123"", fn: function(){alert(this.pass);} }; return person; }"
Dim x : Set x = sc.eval("foo()")

AB.Use "jsLib"
Dim jsLib : Set jsLib = AB.jsLib.New
jsLib.Inc("json2.js") '默认的文件路径为：“AspBox/jsLib/json2.js”
Dim jso : Set jso = jsLib.Object
Dim temp : temp = jso.JSON.stringify(x) '调用JSON.stringify()方法将JS对象转换成为 JSON
AB.C.Print temp '@print: {"name":{"a":"zhangsan"},"pass":"123"}

Dim jsontext : jsontext = "{""name"": ""张三"", ""gender"": ""男"", ""result"": 65}"
ab.trace(jso.JSON.parse(jsontext)) '@desc: vartype(jso.JSON.parse(jsontext)) = vbString and typename(jso.JSON.parse(jsontext)) = "JScriptTypeInfo"
Dim s : Set s  = jso.JSON.parse(jsontext)
ab.trace(s)  '@desc: vartype(s) = vbString and typename(s) = "String" and s = "[object Object]"
%>